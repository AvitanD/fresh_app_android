package com.example.fresh.utils;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import android.util.Log;

import com.example.fresh.StaticVars;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;

import static com.example.fresh.StaticVars.ServerConfigurations.SERVER_IP;
import static com.example.fresh.StaticVars.ServerConfigurations.SERVER_PORT;

/**
 * A class that runs commands on the server and gets responses.
 * The server communication is done on a new thread.
 * A result of -1 means there was an issue connecting to the server / executing the command.
 *
 */
public class ServerHelper implements Runnable {
    private volatile Object result;
    private String command;
    private Socket socket;

    private static final String TAG = StaticVars.TAG + "::serverHelper";
    public ServerHelper (){
        result = "-2";
    }

    /**
     * Command builder - gets command and constructs a json object according to the command type.
     * @param _command: scan/search.
     * @param _data: barcode/ingredients.
     * @param urgent: ingredients about to expire this week.
     */
    public void buildCommand(String _command, String _data, String urgent) {
        JSONObject jsonCommand = new JSONObject();
        try {
            jsonCommand.accumulate("command", _command);
            if(_command.equals("scan"))
                jsonCommand.accumulate("barcode", _data);
            else{
                jsonCommand.accumulate("ingredients", _data);
                jsonCommand.accumulate("urgent", urgent);
            }
        } catch (JSONException e) {
            Log.e(StaticVars.TAG + " serverHandler: ", "error parsing json");
            e.printStackTrace();
        }
        this.command = jsonCommand.toString();
    }


    private Object handleJsonResult(String json){
        String name=null;
        JSONObject jsonObject;
        try {
            jsonObject = (JSONObject)new JSONTokener(json).nextValue();
            name = jsonObject.getString("result");
            Log.i("tag", "got result" + name);
            if(name.equals("recipes")) {
                JSONArray jsonArray = jsonObject.getJSONArray("items");
                for (int i = 0; i < jsonArray.length(); i++) {
                    jsonObject = jsonArray.getJSONObject(i);
                    Log.i("tag", "got data" + jsonObject.getString("name") + ","
                            + jsonObject.getString("link") + " , "
                            + jsonObject.getDouble("score"));
                }
                return jsonArray;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return name;
    }


    public boolean testConnectionToServer(Context context){
        //TODO: Enable this test
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()) {
                try {
                    URL url = new URL(SERVER_IP);
                    HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                    urlc.setConnectTimeout(10 * 1000);
                    urlc.connect();
                    if (urlc.getResponseCode() == 200) {
                        Log.wtf("Connection", "Success !");
                        return true;
                    } else {
                        return false;
                    }
                } catch (MalformedURLException e1) {
                    return false;
                } catch (IOException e) {
                    return false;
                }
            }
            return false;
        }


    public boolean connectToServer() {
        if (socket==null || !socket.isConnected()) {
            try {
                Log.d(TAG, "starting socket");
                InetAddress serverAddr;
                serverAddr = InetAddress.getByName(SERVER_IP);

                socket = new Socket();
                socket.connect(new InetSocketAddress(serverAddr, SERVER_PORT), 10000);

            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, "failed to connect to server.");
                return false;
            }
        }
        return true;
    }

    public void run() {
            try {
                if (!connectToServer()) {
                    this.result = "-1";
                } else {
                    Log.d(TAG, "socket connected");
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                    Log.d(TAG, "writing to server: " + this.command);
                    out.println(this.command);
                    int length;
                    byte[] buffer;
                    buffer = new byte[2048];
                    StringBuilder sb = new StringBuilder();
                    do {
                        InputStream in = socket.getInputStream();
                        ByteArrayOutputStream result = new ByteArrayOutputStream();
                        length = in.read(buffer);
                        Log.d(TAG, "Getting data. Length: " + length + " data: " + new String(buffer));
                        result.write(buffer, 0, length);
                        sb.append(result.toString("UTF-8"));
                    } while (length == buffer.length);
                    String response = sb.toString();
                    result = handleJsonResult(response);
                    Log.d(TAG, "got response " + response);
                    socket.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                this.result = "-1";
            }



    }

    public Object getResult() {
        return result;
    }

}