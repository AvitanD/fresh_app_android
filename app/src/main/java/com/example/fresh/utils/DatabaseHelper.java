package com.example.fresh.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.ContactsContract;
import android.util.Log;


import com.example.fresh.StaticVars;
import com.example.fresh.add_product.Product;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = StaticVars.TAG + "::DBHelper";
    private static final String TABLE_NAME = "products";
    private static final String PK1 = "barcode";
    private static final String PK2 = "expiration";
    private static final String[] COLS = {"name", "amount", "quantity"};
    public static final int DEC_QUANTITY = 1;
    public static final int INC_QUANTITY = 2;

    public DatabaseHelper(Context context) {
        super(context, TABLE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = ("CREATE TABLE " + TABLE_NAME + "(") +
                PK1 + " TEXT," +
                COLS[0] + " TEXT NOT NULL," +
                COLS[1] + " INTEGER NOT NULL," +
                COLS[2] + " INTEGER NOT NULL," +
                PK2 + " DATE," +
                "PRIMARY KEY (barcode,expiration))";
        try {
            db.execSQL(createTable);
        } catch (SQLException sqlException){
            Log.e(TAG, "Failed to create table, " + sqlException.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP IF TABLE EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean addProduct(String barcode, String name, int amount, int quantity, String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PK1, barcode);
        contentValues.put(COLS[0], name);
        contentValues.put(COLS[1], amount);
        contentValues.put(COLS[2], quantity);
        contentValues.put(PK2, date);
        if(getProduct(barcode) != null){
            Log.d(TAG, String.format("%s : %s is already in db. Adding 1.", barcode, name));
            return updateProductQuantity(barcode, DatabaseHelper.INC_QUANTITY);
        } else {
            Log.d(TAG, String.format("Adding product to db : %s %s %d %d %s", barcode, name, amount, quantity, date));
            return db.insert(TABLE_NAME, null, contentValues) != -1;
        }

    }

    /**
     * Returns all of the data from database
     * @return Cursor object containing all items from the products table.
     */
    private Cursor getAlldata(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        return db.rawQuery(query, null);
    }

    private Cursor getThreeUrgentNames()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT name FROM " + TABLE_NAME + " ORDER BY expiration ASC LIMIT 3";
        return db.rawQuery(query, null);
    }

    public List<Product> getAllProducts(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = getAlldata();
        data.moveToFirst();
        int numOfProducts = data.getCount();
        List<Product> productList = new ArrayList<>();
        Log.d(TAG, "Found " + numOfProducts + " products in DB");
        if(numOfProducts == 0)
            return productList;
        do{
            Product product = new Product();
            product.setBarcode(data.getString(0));
            product.setProductName(data.getString(1));
            product.setAmount(data.getInt(2));
            product.setQuantity(data.getInt(3));
            product.setExpirationDate(data.getString(4));
            productList.add(product);
        } while(data.moveToNext());
        db.close();
        return productList;
    }

    public String  getThreeUrgentProducts()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = getThreeUrgentNames();
        StringBuilder names_string = new StringBuilder();
        while(data.moveToNext())
        {
            names_string.append(data.getString(0)).append(",");
        }
        db.close();
        String result_string = names_string.toString();
        return result_string.substring(0,result_string.length()-1);
    }
    /**
     * Returns only the ID that matches the name passed in
     * @param barcode barcode to search for.
     * @return search result
     */
    public Product getProduct(String barcode){
        SQLiteDatabase db = null;
        Cursor data;
        try {
            db = this.getWritableDatabase();

        } catch (SQLException sqlException) {
            Log.e(TAG, "Failed to open db, " + sqlException.getMessage());
        }
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE barcode = '" + barcode + "'";
        Log.i(TAG, "sent query: " + query);
       if (db != null) {
           data = db.rawQuery(query, null);
           if(data.getCount() > 0) {
               data.moveToFirst();
               String name = data.getString(data.getColumnIndex("name"));
               int amount = data.getInt(data.getColumnIndex("amount"));
               int quantity = data.getInt(data.getColumnIndex("quantity"));
               String date = data.getString(data.getColumnIndex("expiration"));
               return new Product(barcode, name, quantity, amount, date);
           }
           data.close();
       }
       return null;

    }

    /**
     * Updates the name field
     * @param newName name to update to
     * @param oldName name to update from
     */
    public void updateName(String newName, String oldName){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_NAME + " SET name = '" + newName + "' WHERE name = '" + oldName + "'";
        Log.d(TAG, "updateName: query: " + query);
        Log.d(TAG, "updateName: Setting name to " + newName);
        db.execSQL(query);
    }

    /**
     * Delete from database
     * @param barcode product barcode
     */
    public boolean deleteProduct(String barcode){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME + " WHERE barcode = '" + barcode +"'";
        Log.d(TAG, "Deleting " + barcode + " from database.");
        try {
            db.execSQL(query);
        } catch (SQLException sqlException){
            Log.e(TAG, "Failed to delete " + barcode);
            return false;
        }
        return true;
    }
    /**
     * Add/Remove a unit of an existing product
     * @param barcode product barcode
     * @param command add/remove
     */
    public boolean updateProductQuantity(String barcode, int command){
        SQLiteDatabase db = null;
        Cursor cursor = null;
        int quantity = 0;
        try {
            db = this.getWritableDatabase();
        } catch (SQLException sqlException) {
            Log.e(TAG, "Falied to open db, " + sqlException.getMessage());
        }
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE barcode = '" + barcode + "'";
        Log.i(TAG, "sent query: " + query);
        if (db != null) {
            cursor = db.rawQuery(query, null);
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                quantity = cursor.getInt(cursor.getColumnIndex("quantity"));
            } else {
                Log.e(TAG, "Tried to delete a product that is not in the db.");
                return false;
            }
            cursor.close();
        }

        if(command == DEC_QUANTITY) {
            if (quantity == 1)
                return deleteProduct(barcode);
            else
                quantity--;
        } else {
            quantity++;
        }
        query = "UPDATE " + TABLE_NAME + " SET quantity = '" + quantity + "' WHERE barcode = '" + barcode + "'";
        Log.d(TAG, "Setting new quantity for " + barcode + " to " + quantity);
        try {
            db.execSQL(query);
            return true;
        } catch (SQLException|NullPointerException sqlException){
            Log.e(TAG, "Failed to change quantity, exception: " + sqlException.getMessage());
            return false;
        }
    }

    /**
     * Delete all products from the products table.
     */
    public void deleteAllProducts(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME;
        Log.i(TAG, "Deleting All products from database.");
        try {
            db.execSQL(query);
        } catch (SQLException sqlException){
            Log.e(TAG, "Failed to delete all products");
        }
    }



}
