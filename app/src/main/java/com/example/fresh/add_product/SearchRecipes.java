package com.example.fresh.add_product;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.fresh.StaticVars;
import com.example.fresh.R;
import com.example.fresh.utils.DatabaseHelper;
import com.example.fresh.utils.ServerHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Sends a request to the server to search for recipes containing  products in the db.
 * The returned recipes are shown in a listview. Clicking a recipe opens it in the browser.
 */
public class SearchRecipes extends AppCompatActivity  {
    static final String TAG = StaticVars.TAG + ":Recipe_search";
    JSONArray recipesJsonArray;
    ListView recipeList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_search);
        recipeList = findViewById(R.id.listview_recipe);
        search_recipes();
        populateListView();
        recipeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = (String)recipeList.getItemAtPosition(position);
                String[] recipe_text = item.split(",");
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(recipe_text[1]));
                startActivity(browserIntent);
            }
        });

    }

    public void search_recipes (){
        DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
        String products = databaseHelper.getThreeUrgentProducts();
        ServerHelper serverHelper = new ServerHelper();
        serverHelper.buildCommand("search",products,products);
        Thread serverThread = new Thread(serverHelper);
        Log.i(TAG, "calling server thread - search recipes with data" + products);
        serverThread.start();


        try {
            serverThread.join(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        recipesJsonArray = (JSONArray) serverHelper.getResult();

        Log.i(TAG, "waiting for server results");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(recipesJsonArray!=null)
            Log.d(TAG, "Got response recipes array");
        else {
            Log.e(TAG, "failed to get result from server");
            Toast.makeText(getApplicationContext(), "A problem occured while getting recipe " +
                    "list. Please try again.", Toast.LENGTH_LONG).show();
        }
    }
    private void populateListView(){
        Log.d(TAG, "populating listview");
        JSONObject jsonObject = new JSONObject();
        ArrayList<String> listData = new ArrayList<>();
        for(int i=0;i<recipesJsonArray.length();i++){
            try {
                jsonObject = recipesJsonArray.getJSONObject(i);
                listData.add(jsonObject.getString("name") + ","
                        + jsonObject.getString("score"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ListAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listData);
        recipeList.setAdapter(adapter);


    }

}
