package com.example.fresh.add_product;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.fresh.StaticVars;
import com.example.fresh.R;
import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * The barcode scanner class uses the ZXingScannerView class which takes care of identifying
 *   multiple barcode formats.
 * The ZXingScannerView handles interactions with the android camera2 API, creating a camera
 *   preview screen along with an animated viewfinder, and scans each frame for black-white pixel
 *   patterns, finding barcode center points.
 * It then sends the verified barcodes to a barcode reader that is capable of identifying barcode
 *   formats and embedded codes based on the ratios between barcode line sizes.
 */
public class BarcodeScanner extends AppCompatActivity implements ZXingScannerView.ResultHandler{
    private ZXingScannerView scannerView;
    private static final String TAG = StaticVars.TAG + ":Barcode_Scanner";
    private final int VERIFIER_REQUEST_CODE = 3;
    String barcode;
    Button manualButton;

    /**
     * Called on initialization of this activity.
     */
    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.content_barcode_scanner);
        ViewGroup contentFrame = findViewById(R.id.content_frame);
        scannerView = new ZXingScannerView(this);
        contentFrame.addView(scannerView);
        manualButton = findViewById(R.id.button_manual_barcode);
        manualButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Going to manual insertion");
                scannerView.stopCameraPreview();
                scannerView.stopCamera();
                callVerifierActivity();
            }
        });
    }

    /**
     * Called upon opening the app after it was sent to background
     */
    @Override
    public void onResume() {
        super.onResume();
        scannerView.setResultHandler(this);
        scannerView.startCamera();
        //TODO: Change to front facing camera and flip the camera view.
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

    /**
     * Gets called when a result is received from the ZXingScanner
     * @param rawResult - an object containing the type and code retrieved from the camera frame.
     */
    @Override
    public void handleResult(Result rawResult) {
        barcode = rawResult.getText();
        Toast.makeText(this, "Contents = " + barcode +
                ", Format = " + rawResult.getBarcodeFormat().toString(), Toast.LENGTH_SHORT).show();

        Log.d(TAG, "Calling barcode verifier dialog.");
        callVerifierActivity();

        // Wait 2 seconds to resume the preview (prevents application hangs in weaker devices).
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                scannerView.resumeCameraPreview(BarcodeScanner.this);
            }
        }, 2000);
    }

    /**
     * Send the recieved date to a verification window where it can be changed.
     * This will be called with a blank barcode in case the user selected a manual insertion.
     */
    private void callVerifierActivity(){
        Intent verifier_intent = new Intent(getApplicationContext(), BarcodeVerifier.class);
        verifier_intent.putExtra("barcode", barcode);
        startActivityForResult(verifier_intent, VERIFIER_REQUEST_CODE);
    }

    /**
     * Listener for returning from the verification activity.
     * Sets this activity's return code to 1 (success) and attaches the recieved barcode.
     * @param requestCode - identifying code for the returning activity.
     * @param resultCode - the result of the activity
     * @param data - any attached data from the activity.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "Barcode verified, returning");
        if(requestCode == VERIFIER_REQUEST_CODE && resultCode > 0){
            if (data != null) {
                barcode = data.getStringExtra("new_barcode");
            }
            Intent intent = getIntent();
            intent.putExtra("barcode", barcode );
            setResult(1, intent);
            finish();
        }
    }


}
