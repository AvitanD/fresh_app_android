package com.example.fresh.add_product;

public class Recipe {
    private String name,url;
    private float rank;

    public float getRank()
    {
        return rank;
    }

    public String getName()
    {
        return name;
    }

    public String getUrl()
    {
        return url;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setRank(float rank)
    {
        this.rank = rank;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }
}
