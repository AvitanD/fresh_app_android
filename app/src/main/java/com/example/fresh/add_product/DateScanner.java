package com.example.fresh.add_product;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.fresh.R;
import com.example.fresh.StaticVars;
import com.googlecode.tesseract.android.TessBaseAPI;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCamera2View;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.KeyPoint;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

public class DateScanner extends AppCompatActivity {
    private static final int DATE_SCANNER_REQUEST_CODE = 19;
    private boolean isTest = false;
    private static String TAG = StaticVars.TAG + ":DateScan:";
    private TessBaseAPI tesseractBaseAPI;
    private int frameCounter = 0;
    private SurfaceView processedImageView;
    private SurfaceHolder holder;
    private char dateDividingChar;
    private Mat inputMatrix;
    private String date;
    JavaCamera2View javaCameraView;
    BaseLoaderCallback baseLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            if (status == BaseLoaderCallback.SUCCESS && !isTest) {
                javaCameraView.enableView();
            } else {
                super.onManagerConnected(status);
            }
            super.onManagerConnected(status);
        }
    };

    static {
        if (OpenCVLoader.initDebug()) {
            Log.i(TAG, "OpenCV loaded successfully");
        } else {
            Log.i(TAG, "failed to load OpenCV");
        }
    }

    // App lifecycle functions/listeners
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_scanner);
        Button manualButton = findViewById(R.id.button_manual_date);
        manualButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callDateVerifierActivity();
            }
        });
        processedImageView = findViewById(R.id.processing_view);

        holder = processedImageView.getHolder();
        holder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                Log.i(TAG, "SurfaceHolder Created");
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

            }
        });

        //Try to activate Tesseract engine with our trained language files to detect text in the frame.
        try {
            prepareTessData();
            initTessract();
        } catch (IOException | NullPointerException | IllegalArgumentException tesseractInitException){
            Log.e(TAG, "Error initializing tesseract : " + tesseractInitException.getMessage());
            Toast.makeText(getApplicationContext(), "Unable to initiate Tesseract engine. Can't " +
                    "proceed with date scan", Toast.LENGTH_LONG).show();
            callDateVerifierActivity();
        }
        Intent calling_intent = getIntent();
        try {
            isTest = Objects.requireNonNull(calling_intent.getExtras()).getBoolean("test");
        } catch (NullPointerException e) {
            Log.w(TAG, "This is NOT A TEST 0_0");
        }
        if (!isTest) {
            javaCameraView = findViewById(R.id.java_camera_view);
            javaCameraView.setVisibility(SurfaceView.VISIBLE);
            javaCameraView.setCvCameraViewListener(cam_view_listener);
        } else {
            Handler handler = new Handler();
            handler.postDelayed(testDateScanner, 1000);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (javaCameraView != null)
            javaCameraView.disableView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (javaCameraView != null)
            javaCameraView.disableView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (OpenCVLoader.initDebug()) {
            Log.i(TAG, "OpenCV loaded successfully");
            baseLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        } else {
            Log.i(TAG, "failed to load OpenCV");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, baseLoaderCallback);
        }
    }


    private Bitmap mat2Bitmap(Mat origin) {
        Bitmap bmp;
        bmp = Bitmap.createBitmap(origin.width(), origin.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(origin, bmp);
        return bmp;
    }

    /**
     * For debugging - show the post-processing image
     * @param img .
     */
    private void drawBitmapOnProcessView(Mat img) {
        Bitmap bmp = mat2Bitmap(img);
        Canvas c = holder.lockCanvas();
        Bitmap rescaled = Bitmap.createScaledBitmap(bmp, processedImageView.getWidth(), processedImageView.getHeight(), false);
        c.drawBitmap(rescaled, 0, 0, null);
        holder.unlockCanvasAndPost(c);
    }

    /**
     * Run a test for the date scanner, testing it on pretaken images in /assets/test_imgs.
     * To activate, call this activity as an intent with extra boolean named "test" set to True.
     */
    private Runnable testDateScanner = new Runnable() {
        public void run() {
            AssetManager assetManager = getAssets();
            String DSTESTTAG = "TestDateScan:";
            String[] files;
            Mat original_mat;
            Bitmap bmp;
            InputStream istr;
            try {
                files = assetManager.list("test_imgs");
                assert files != null;
                Log.i(DSTESTTAG, "Starting tests:");
                for (String x : files) {
                    Log.i(DSTESTTAG, "current file: " + x);
                    istr = assetManager.open("test_imgs/" + x);
                    bmp = BitmapFactory.decodeStream(istr);
                    original_mat = new Mat();
                    Utils.bitmapToMat(bmp, original_mat);
                    long sTime = System.currentTimeMillis();
                    boolean res = adaptiveScan(original_mat);
                    long eTime = System.currentTimeMillis();
                    if (res)
                        Log.i(TAG, "Got the date in " + ((eTime - sTime)) + "mS");
                    else
                        Log.i(TAG, "Failed to get date, time wasted:  " + ((eTime - sTime)) + "mS");
                    Thread.sleep(2000);
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }

        /* Add the below code to main activity (connected to a test button) to run the test procedure:
        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Going into test mode");
                Intent myIntent = new Intent(getApplicationContext(), DateScanner.class);
                myIntent.putExtra("test", true); //Optional parameters
                startActivity(myIntent);
            }
        });
         */
    };

    // A listener to get frames from the camera once opened.
    CameraBridgeViewBase.CvCameraViewListener2 cam_view_listener = new CameraBridgeViewBase.CvCameraViewListener2() {
        /**
         * Once camera is opened, get the width and height and prepare a compatible blank matrix.
         * @param width -  the width of the frames that will be delivered
         * @param height - the height of the frames that will be delivered
         */
        @Override
        public void onCameraViewStarted(int width, int height) {
            inputMatrix = new Mat(height, width, CvType.CV_8UC4);
        }

        @Override
        public void onCameraViewStopped() {
            inputMatrix.release();
        }

        /**
         * Listens for frames from the camera. .
         * Converts the frame to a matrix, and paints an ROI rectangle for the user.
         * Sends the ROI of every 10th frame (to save processing time) to processing and if a date
         *      is found, sends it to the DateVerifier activity.
         * As each camera has its own image size, the ROI is dynamic.
         * @param inputFrame a frame from the camera.
         * @return the input frame with the ROI rectangle.
         */
        @Override
        public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
            frameCounter = (frameCounter + 1) % 10;
            Mat cropped_mat;
            date = "";
            inputMatrix = inputFrame.rgba();
            Point topLeft = new Point((inputMatrix.width() / 2.0) - (inputMatrix.width() / 5.0),
                    (inputMatrix.height() / 2.0) - inputMatrix.height() / 8.0);
            Point bottomRight = new Point((inputMatrix.width() / 2.0) + (inputMatrix.width() / 5.0)
                    , (inputMatrix.height() / 2.0) + inputMatrix.height() / 8.0);
            Rect roi = new Rect(topLeft, bottomRight);
            Imgproc.rectangle(inputMatrix, topLeft, bottomRight, new Scalar(255, 0, 0), 8);
            if (inputMatrix != null && frameCounter == 0) {
                Log.d(TAG, "frame " + frameCounter + ", processing..");
                try {
                    cropped_mat = inputMatrix.submat(roi);
                } catch (CvException exception){
                    Log.e(TAG, String.format("Error creating ROI submatrix: %s", exception.toString()));
                    throw exception;
                }
                boolean res = adaptiveScan(cropped_mat);
                if (res) {
                    Log.d(TAG, "Got text: " + date);
                    Log.d(TAG, "Sending " + date + " to date verifier.");
                    callDateVerifierActivity();
                }
            }
            return inputMatrix;
        }
    };

    /**
     * Send the recieved date (or a blank string) and send to the verifier dialog.
     */
    private void callDateVerifierActivity() {
        Intent verifier = new Intent(getApplicationContext(), DateVerifier.class);
        verifier.putExtra("date", date);
        verifier.putExtra("divider", String.valueOf(dateDividingChar));
        startActivityForResult(verifier, DATE_SCANNER_REQUEST_CODE);
    }

    /**
     * When returning from the verifier activity, take the user-entered date and send back to the calling activity.
     * @param requestCode: Identifies the returning activity.
     * @param resultCode: Success/failure.
     * @param data: the returned data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DATE_SCANNER_REQUEST_CODE && resultCode > 0) {
            String date = null;
            if (data != null) {
                date = data.getStringExtra("new_date");
            }
            Intent intent = getIntent();
            intent.putExtra("date", date);

            setResult(1, intent);
            finish();
        }
    }

    /**
     * Run further examination of the string retrieved by TesseractOCR, to filter random/wrong strings.
     *  Checks the retrieved date format (by length - DD/MM/YYYY, DD/MM/YY, DD/MM).
     *  Verifies that all chars are alpha-numeric (by trying to parse to int).
     *  Checks that the date is correct (up to 31 days, 12 months).
     *  Checks that the date is reasonable (up to 4 years ahead).
     * @param tesseractOutputString: The string retrieved by TesseractOCR
     * @return true/false.
     */
    public boolean dateLogicCheck(String tesseractOutputString) {
        //Date format checks (either dd/mm/yy or dd/mm/yyyy with seperators / or - or .)
        int day,month,year;

        int date_type;
        switch(tesseractOutputString.length()){
            case 4:
                date_type = StaticVars.DateFormat.NO_YEAR;
                break;
            case 8:
                date_type = StaticVars.DateFormat.SHORT_YEAR_YY;
                break;
            case 10:
                date_type = StaticVars.DateFormat.LONG_YEAR_YYYY;
                break;
            default:
                return false;
        }
        dateDividingChar = tesseractOutputString.charAt(2);
        if (!(dateDividingChar == '/' || dateDividingChar == '-' || dateDividingChar == '.'))
            return false;
        String[] split_date = tesseractOutputString.split(String.valueOf(dateDividingChar));
        if(date_type == StaticVars.DateFormat.LONG_YEAR_YYYY){
            if (split_date.length < 3)
                return false;
            try{
                day = Integer.valueOf(split_date[0]);
                month = Integer.valueOf(split_date[1]);
                year = Integer.valueOf(split_date[2]) % 100; //Get YY from YYYY
            } catch (NumberFormatException nf_exc){
                return false;
            }
        } else if (date_type == StaticVars.DateFormat.SHORT_YEAR_YY){
            if (split_date.length < 2)
                return false;
            try{
                day = Integer.valueOf(split_date[0]);
                month = Integer.valueOf(split_date[1]);
                year = Integer.valueOf(split_date[2]);
            } catch (NumberFormatException nf_exc){
                return false;
            }
        } else {
            if (split_date.length < 2)
                return false;
            try{
                day = Integer.valueOf(split_date[0]);
                month = Integer.valueOf(split_date[1]);
                year = Calendar.getInstance().get(Calendar.YEAR);
                Log.e(TAG, "YEAR FORMAT : " + year);
            } catch (NumberFormatException nf_exc){
                return false;
            }
        }

        if (day > 31 || month > 12)
            return false;
        int current_year = Calendar.getInstance().get(Calendar.YEAR);
        return (year - current_year % 100) <= 4;
    }

    /**
     * Copies the Tesseract trained .dat files from the projects' assets folder
     *           and saves them into the local device storage (if they don't already exist).
     */
    private void prepareTessData() throws IOException, NullPointerException{

            File dir = new File(StaticVars.DATA_PATH + StaticVars.TESS_DATA);
            if (!dir.exists()) {
                boolean mkdir_res = dir.mkdirs();
                if (!mkdir_res)
                    throw new FileNotFoundException("couldn't create folder: " + dir.getPath());
                else
                    Log.d(TAG, "Folder " + dir.getPath() + "created successfully.");
            }
            String[] fileList = getAssets().list("");
            for (String fileName : Objects.requireNonNull(fileList)) {
                if (fileName.endsWith("traineddata")) {
                    String pathToDataFile = StaticVars.DATA_PATH + StaticVars.TESS_DATA + "/" + fileName;
                    if (!(new File(pathToDataFile)).exists()) {
                        InputStream in_stream = getAssets().open(fileName);
                        OutputStream os = new FileOutputStream(pathToDataFile);
                        byte[] buff = new byte[1024];
                        int len;
                        while ((len = in_stream.read(buff)) > 0) {
                            os.write(buff, 0, len);
                        }
                        in_stream.close();
                        os.close();
                    }
                }
            }

    }

    //TODO maybe use this
    private void detectText() {
        /**
         * @param: Mat mat
         * Finds text in mat by finding contours and calculating sizes.
         * Displays the text in a Toast.
         */


        Mat mGray = new Mat(inputMatrix.rows(), inputMatrix.cols(), CvType.CV_8UC4);
        Imgproc.cvtColor(inputMatrix, mGray, Imgproc.COLOR_RGB2GRAY);
        Scalar CONTOUR_COLOR = new Scalar(1, 255, 128, 0);
        MatOfKeyPoint keyPoint = new MatOfKeyPoint();
        List<KeyPoint> listPoint = new ArrayList<>();
        KeyPoint kPoint = new KeyPoint();

        int rectanx1;
        int rectany1;
        int rectanx2;
        int rectany2;

        Scalar zeros = new Scalar(0, 0, 0);
        List<MatOfPoint> contour2 = new ArrayList<>();
        Mat kernel = new Mat(1, 50, CvType.CV_8UC1, Scalar.all(255));
        Mat morByte = new Mat();
        Mat hierarchy = new Mat();

        Rect rectan3 = new Rect();
        Rect rect = new Rect(new Point(((double)mGray.width() / 4), (double)mGray.height() / 4), new Point(2 * (double)mGray.width() / 4, 2 * (double)mGray.height() / 4));
        Mat cropped = mGray.submat(rect);
        Imgproc.threshold(cropped, cropped, 40.0, 255, Imgproc.THRESH_BINARY);
        Bitmap pbmp = Bitmap.createBitmap((int) cropped.size().width, (int) cropped.size().height, Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(cropped, pbmp);


        Mat mask = Mat.zeros(cropped.size(), CvType.CV_8UC1);
        FeatureDetector detector = FeatureDetector.create(FeatureDetector.MSER); // MSER is a blob detection algorithm
        detector.detect(cropped, keyPoint);
        listPoint = keyPoint.toList();
        for (int ind = 0; ind < listPoint.size(); ++ind) {
            kPoint = listPoint.get(ind);
            rectanx1 = (int) (kPoint.pt.x - 0.5 * kPoint.size);
            rectany1 = (int) (kPoint.pt.y - 0.5 * kPoint.size);

            rectanx2 = (int) (kPoint.size);
            rectany2 = (int) (kPoint.size);
            if (rectanx1 <= 0) {
                rectanx1 = 1;
            }
            if (rectany1 <= 0) {
                rectany1 = 1;
            }
            if ((rectanx1 + rectanx2) > cropped.width()) {
                rectanx2 = cropped.width() - rectanx1;
            }
            if ((rectany1 + rectany2) > cropped.height()) {
                rectany2 = cropped.height() - rectany1;
            }
            Rect rectant = new Rect(rectanx1, rectany1, rectanx2, rectany2);
            Mat roi = new Mat(mask, rectant);
            roi.setTo(CONTOUR_COLOR);
        }

        Imgproc.morphologyEx(mask, morByte, Imgproc.MORPH_DILATE, kernel);
        Imgproc.findContours(morByte, contour2, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE);
        Bitmap bmp = null;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < contour2.size(); ++i) {
            rectan3 = Imgproc.boundingRect(contour2.get(i));
            try {
                Mat croppedPart = mGray.submat(rectan3);
                bmp = Bitmap.createBitmap(croppedPart.width(), croppedPart.height(), Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(croppedPart, bmp);
                Canvas c = holder.lockCanvas();
                Bitmap rescaled = Bitmap.createScaledBitmap(bmp, processedImageView.getWidth(), processedImageView.getHeight(), false);
                c.drawBitmap(rescaled, 0, 0, null);
                holder.unlockCanvasAndPost(c);
            } catch (Exception e) {
                Log.d(TAG, "Cropped part error");
            }
            if (bmp != null) {
                String str = getTextWithTesseract(bmp);
                if (str != null) {
                    sb.append(str).append("\n");
                }
            }
        }
    }

    /**
     * Initialize the Tesseract OCR engine with our trained data files and specific settings:
     *  pageseg_mode : scan for single-line text.
     *  var_char_whitelist : the characters to look for (all others will be ignored).
     *  If initialization fails, exception will be thrown
     */
    private void initTessract() throws IllegalArgumentException{
        Log.d(TAG, "Starting Tesseract");
        tesseractBaseAPI = new TessBaseAPI();
        tesseractBaseAPI.init(StaticVars.DATA_PATH, "dig+dat+myl", TessBaseAPI.OEM_DEFAULT);
        if (!tesseractBaseAPI.setVariable("tessedit_pageseg_mode", "7"))
            Log.w(TAG, "Couldn't set segmentation mode, using default");
        if (!tesseractBaseAPI.setVariable("VAR_CHAR_WHITELIST", "123456789/.-"))
            Log.w(TAG, "Couldn't set character whitelist, recognizing all.");

    }

    /**
     * Send an image to be scanned by tesseract.
     * @param bitmap the image to be scanned
     * @return the extracted string
     */
    private String getTextWithTesseract(Bitmap bitmap) {
        tesseractBaseAPI.setImage(bitmap);
        long sTime = System.currentTimeMillis();
        String retStr = tesseractBaseAPI.getUTF8Text();
        long eTime = System.currentTimeMillis();
        Log.d("TessTime", "Time to get string  : " + (eTime - sTime));
        return retStr;
    }

    /**
     * An adaptive scan algorithm:
     *  Gets the current image as a Matrix.
     *  Applies grayscale, calculates an appropriate blur radius, applise adaptive thresholding.
     *  Runs a morph_close algorithm (dilation followed with erosion) on the image with varying
     *      kernel sizes, until a result is recieved or the maximum kernel size was reached.
     *  At each iteration, the processed image is sent to the tesseract engine and to the logical
     *      date checker to try and extract the date.
     * @param inputImage: the current frame as a Matrix
     * @return true if succeeded, false otherwise.
     */
    private boolean adaptiveScan(Mat inputImage) {
        Mat mGray = new Mat(inputImage.size(), CvType.CV_8UC4);
        Mat kernel;
        Bitmap bmp;
        boolean res;
        Imgproc.cvtColor(inputImage, mGray, Imgproc.COLOR_RGB2GRAY);
        Size blur_radius;
//        if (mGray.width() > 1000 && mGray.height() > 1000) {
//            blur_radius = new Size((double) mGray.width() / 1000, (double) mGray.height() / 1000);
//
//        } else {
//            blur_radius = new Size((double) mGray.width() / 2, (double) mGray.height() / 2);
//            Log.i(TAG, "Setting blur to : " + (blur_radius.width));
//        }

        Imgproc.blur(mGray, mGray, new Size(5, 5));
        Mat processed = new Mat(mGray.size(), CvType.CV_8UC1);
        //TODO: Add test if text is white - inverted thresh binary
        Imgproc.adaptiveThreshold(mGray, processed, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C,
                Imgproc.THRESH_BINARY, 5 * 5, 30);
        for (int i = 5; i < 18; i = i + 2) {
            long sTime = System.currentTimeMillis();
            kernel = new Mat(i, i/2, CvType.CV_8UC1);
            Imgproc.adaptiveThreshold(mGray, processed, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C,
                    Imgproc.THRESH_BINARY, 5 * 5, 30);
            Imgproc.morphologyEx(processed, processed, Imgproc.MORPH_OPEN, kernel);
            drawBitmapOnProcessView(processed);
            bmp = mat2Bitmap(processed);
            long cStime = System.currentTimeMillis();
            date = getTextWithTesseract(bmp);
            Log.e(TAG, "convert time: " + (System.currentTimeMillis() - cStime) + "ms");
            Log.d(TAG, "Raw text from image: " + date);
            res = dateLogicCheck(date);
            Log.e(TAG, "loop time: " + (System.currentTimeMillis() - sTime)/1000.0 + "s");
            if (res) {
                Log.i(TAG, "Got date: " + date);
                tesseractBaseAPI.end();
                return true;
            } else {
                Log.d(TAG, "increasing kernel size to: " + kernel.height() + " x " + kernel.width());
            }
        }
        return false;

    }
}

