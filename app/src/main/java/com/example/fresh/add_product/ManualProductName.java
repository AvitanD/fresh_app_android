package com.example.fresh.add_product;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.fresh.StaticVars;
import com.example.fresh.R;

/**
 * In case a product name could not be fetched from server, allows the user to manually enter it.
 */
public class ManualProductName extends AppCompatActivity {
    EditText txtFieldProductName;
    Button approve;
    String productName;
    private static final String TAG = StaticVars.TAG + "::Add_prod::get_manual_name";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_manual_product_name);
        txtFieldProductName = findViewById(R.id.txt_name);
        Intent calling_intent = getIntent();
        txtFieldProductName.setText(calling_intent.getStringExtra("Name"));
        approve = findViewById(R.id.button_approve);
        approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productName = txtFieldProductName.getText().toString();
                Intent calling_intent = getIntent();
                calling_intent.putExtra("name", productName);
                setResult(1, calling_intent);
                finish();
            }
        });
    }
}
