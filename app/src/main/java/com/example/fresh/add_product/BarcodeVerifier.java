package com.example.fresh.add_product;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.fresh.R;

/**
 * Activity that gets the scanned barcode (or an empty string if user chose to manually insert the barcode),
 * and allows the user to fix/re-enter the barcode manually.
 * Layout is defined as dialog so the activity appears on top (and not instead) of the calling one .
 */
public class BarcodeVerifier extends AppCompatActivity {
    String barcode;
    TextView textFieldBarcode;
    Button approve;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_barcode_verifier);
        textFieldBarcode = findViewById(R.id.txt_barcode);
        approve = findViewById(R.id.button_approve);
        intent = getIntent();
        barcode = intent.getStringExtra("barcode");
        textFieldBarcode.setText(barcode);
        approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                barcode = textFieldBarcode.getText().toString(); // getText returns a CharSequence
                setResult(1, intent);
                intent.putExtra("new_barcode", barcode);
                finish();
            }
        });


    }
}
