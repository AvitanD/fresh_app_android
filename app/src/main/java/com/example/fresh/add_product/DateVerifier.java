package com.example.fresh.add_product;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;

import com.example.fresh.StaticVars;
import com.example.fresh.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Gets the date retrieved by DateScanner, displays it to the user in a NumberPicker, and allows
 *  the user to change the selected date.
 * If the date recieved from DateScanner is empty, shows the user today's date.
 * Returns the user-selected date when clicking the confirm button.
 */
public class DateVerifier extends AppCompatActivity {
    NumberPicker numberPickerDay, numberPickerMonth, numberPickerYear;
    Button approve;
    Intent calling_intent;
    private static final String TAG = StaticVars.TAG + ":DateScanner:V";
    String date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_date_verifier);
        numberPickerDay =findViewById(R.id.np_day);
        numberPickerMonth =findViewById(R.id.np_month);
        numberPickerYear =findViewById(R.id.np_year);
        numberPickerDay.setMinValue(0);
        numberPickerDay.setMaxValue(31);
        numberPickerMonth.setMinValue(0);
        numberPickerMonth.setMaxValue(12);
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        numberPickerYear.setMinValue(currentYear);
        numberPickerYear.setMaxValue(currentYear+4);
        calling_intent = getIntent();
        date = calling_intent.getStringExtra("date");

        String divider = calling_intent.getStringExtra("divider");
        if(date.equals("")){
            Date todayDate = Calendar.getInstance().getTime();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy", Locale.getDefault());
            date = formatter.format(todayDate);
            divider= "-";
        }
        String[] date_split = date.split(divider);
        numberPickerDay.setValue(Integer.valueOf(date_split[0]));
        numberPickerMonth.setValue(Integer.valueOf(date_split[1]));
        numberPickerYear.setValue(Integer.valueOf(date_split[2]));
        approve = findViewById(R.id.button_approve);
        approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String new_date = numberPickerYear.getValue() + "-" + numberPickerMonth.getValue() + "-" + numberPickerDay.getValue();
                Log.d(TAG, "Verified date: " + new_date);
                setResult(1, calling_intent);
                calling_intent.putExtra("new_date", new_date);
                finish();
            }
        });
    }
}
