package com.example.fresh.add_product;


public class Product {

    private String barcode, expirationDate, productName;
    private int amount, quantity; // amount = weight/liters of product, quantity - how much in inv.

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public void setProductName(String name) {
        this.productName = name;
    }


    private String padRight(String inputString, int paddingAmount) {
        StringBuilder sb = new StringBuilder();
        sb.append(inputString);
        for (int i=0; i < paddingAmount; i++)
            sb.append(" ");
        return sb.toString();
    }

    public Product(){
        productName = null;
        barcode = null;
        amount = 0;
        quantity = 0;
        expirationDate = null;

    }

    /**
     * Builder with parameters
     * @param barcode String
     * @param name String
     * @param quantity Integer
     * @param amount Integer
     * @param date String
     */
    public Product(String barcode, String name, int quantity, int amount, String date){
        this.barcode = barcode;
        this.productName = name;
        this.quantity = quantity;
        this.amount = amount;
        this.expirationDate = date;
    }

    /**
     * Print product details. Meant for use in the list view, padded for formatting purposes.
     * @return product details - barcode, name, amount (e.g. weight, volume..), quantity.
     */
    @Override
    public String toString() {
        return padRight(this.barcode, 17-this.barcode.length()) +
                padRight(this.productName, 20 - this.productName.length()) +
                padRight(String.valueOf(this.amount),3 - String.valueOf(this.amount).length()) +
                padRight(String.valueOf(this.quantity),3 - String.valueOf(this.amount).length())
                + this.expirationDate;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getBarcode() {
        return barcode;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public String getName() {
        return productName;
    }

    public int getAmount() {
        return amount;
    }

    public int getQuantity() {
        return quantity;
    }


}
