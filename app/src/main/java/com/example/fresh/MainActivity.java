package com.example.fresh;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fresh.add_product.BarcodeScanner;
import com.example.fresh.add_product.DateScanner;
import com.example.fresh.add_product.ManualProductName;
import com.example.fresh.add_product.Product;
import com.example.fresh.add_product.SearchRecipes;
import com.example.fresh.utils.DatabaseHelper;
import com.example.fresh.utils.ServerHelper;

import java.util.Arrays;
import java.util.Calendar;

/**
 * This is the main activity displayed upon opening the application.
 * Contains a menu to launch selected activities (add products, search recipies etc.) ,
 * and a list of current items in the local db.
 */
public class MainActivity extends AppCompatActivity {
    static final String TAG = StaticVars.TAG + ":Main";
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final int REQUEST_STORAGE_PERMISSION = 2;
    private static final int BARCODE_REQ_CODE = 1;
    private static final int DATE_REQ_CODE = 2;
    private static final int NAME_REQ_CODE = 3;
    private static final int SEARCH_RECIPES = 1;
    ServerHelper serverHelper;
    Button scan, search, finish, deleteAll;
    Product product;
    Intent intent;
    DatabaseHelper dbHelper;
    ListView productList;
    Product selectedProduct;
    TextView title;

    /**
     * Verify that the app has camera access.
     * If not, and the user already declined once, show a message explaining why permission is needed.
     */
    private void checkCameraPermissions() {
        Log.i(TAG, "Checking permissions for camera ");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Got permission, opening camera");
        } else {
            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                Toast.makeText(getApplicationContext(), "This app requires camera access to scan products.",
                        Toast.LENGTH_LONG).show();
            }
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        }
    }

    /**
     * Verify that the app has storage access.
     * If not, and the user already declined once, show a message explaining why permission is needed.
     */
    private void checkStoragePermissions() {
        Log.i(TAG, "Checking permissions for storage ");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Got permission for storage");
        } else {
            if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE))
                Toast.makeText(getApplicationContext(), "This app requires storage access to store scanned items information.",
                        Toast.LENGTH_LONG).show();
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSION);
        }
    }

    //TODO add connectivity check
    private void checkServerConnection() {
        if (!serverHelper.connectToServer()) {
            Log.e(TAG, "Failed to connect to server");
            Toast.makeText(getApplicationContext(), "Can't add products without server connection", Toast.LENGTH_LONG).show();
            scan.setEnabled(false);
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    /**
     * Listener that catches configuration events (such as orientation changes).
     * Used here to prevent re-initing the app.
     * @param newConfig - the new status the device is in.
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        title = findViewById(R.id.title_msg);
        scan = findViewById(R.id.button_scan);
        search = findViewById(R.id.button_search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchRecipes();
            }
        });
        deleteAll = findViewById(R.id.button_delete_all);
        deleteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbHelper.deleteAllProducts();
                populateListView();
            }
        });
        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_product();
            }
        });
        finish = findViewById(R.id.button_finish);
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        serverHelper = new ServerHelper();
        dbHelper = new DatabaseHelper(getApplicationContext());
        productList = findViewById(R.id.products_list_view);
        // Read all products from local db and make them clickable.
        populateListView();
        //TODO: add click action.
        productList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = productList.getItemAtPosition(position).toString();
                final String[] item_details = item.split("\\s+"); //regex to split by any amount of spaces.
                for (String mItem : item_details) {
                    Log.i(TAG, "clicked item " + mItem);
                }
                PopupMenu popup = new PopupMenu(getApplicationContext(), view);
                popup.inflate(R.menu.popup_menu);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();
                        switch(id){
                            case (R.id.delete_all):
                                Log.i(TAG, "Deleting " + item_details[0] + ": " + item_details[1]);
                                dbHelper.deleteProduct(item_details[0]);
                                populateListView();
                                return true;
                            case (R.id.delete_one):
                                Log.i(TAG, "Removing one " + item_details[1]);
                                dbHelper.updateProductQuantity(item_details[0], DatabaseHelper.DEC_QUANTITY);
                                populateListView();
                                return true;
                            default:
                                return false;
                        }
                    }
                });
                popup.show();

            }
        });
        checkCameraPermissions();
        checkStoragePermissions();
        setTitleMessage();
    }

    /**
     * Sets a user welcome message, to be displayed at the top of the screen.
     */
    private void setTitleMessage(){
        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        String name = "דור"; //TODO: Add getting the name from the user.
        String message = "";
        if (hour > 5 && hour < 12) message+="בוקר טוב ";
        else
            if (hour <18 && hour > 12) message += "צהריים טובים ";
            else
                if (hour < 23 && hour > 18) message+= "ערב טוב ";
                else
                    message+= "לילה טוב ";
        message += name;
        title.setText(message);
    }

    /**
     * Gets the current list of items in the local sqlite db, using the dbHelper class.
     */
    private void populateListView() {
        Log.d(TAG, "populating listview.. ");
        ListAdapter adapter = new ArrayAdapter<>(this, R.layout.list_item_layout, dbHelper.getAllProducts());
        productList.setAdapter(adapter);
    }

    /**
     * Calls the recipe search activity
     */
    private void searchRecipes() {
        Log.i(TAG, "calling searchRecipes");
        intent = new Intent(getApplicationContext(), SearchRecipes.class);
        startActivity(intent);
    }

    /**
     * Starting point for the add-product procedure.
     * Initialize a new product object and call the BarcodeScanner activity to get the barcode.
     */
    private void add_product() {
        product = new Product();
        Log.i(TAG, "calling barcode scanner");
        intent = new Intent(getApplicationContext(), BarcodeScanner.class);
        startActivityForResult(intent, BARCODE_REQ_CODE);
    }

    /**
     * Listener called whenever an activity (called from this activity) finishes its work and returns.
     * Because we are using multiple activities for dates, barcodes, and product name retrieval,
     *  this listener will be called multiple times with different return codes.
     *  It then calls the next activity in line (Barcode>Date>Name*).
     *  Note; If a product already exists in the local db (by barcode), the Name activity will not be called.
     * For each scan (BC & Date) a manual verifier activity is also called.
     * The product name retrieval is performed on a parallel thread once the BC is acquired,
     *  as it can take some time to fetch the name from the server.
     * @param requestCode: identifies the activity returned (BC/Date/Name).
     * @param AcitivityResultCode: Did the activity work fine.
     * @param data: any extra data returned by the activity.
     */
    @Override
    protected void onActivityResult(int requestCode, int AcitivityResultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, AcitivityResultCode, data);
        String product_name = null;
        try {
            switch (requestCode) {
                case (BARCODE_REQ_CODE):
                    if (AcitivityResultCode > 0) {
                        String returnedBarcode = null;
                        if (data != null) {
                            returnedBarcode = data.getStringExtra("barcode");
                        }
                        Log.i(TAG, "Got barcode result: " + returnedBarcode);
                        product.setBarcode(returnedBarcode);
                        Product productWithBarcodeInInventory = dbHelper.getProduct(returnedBarcode);
                        if(productWithBarcodeInInventory == null) {
                            serverHelper.buildCommand("scan", returnedBarcode, null);
                            Thread runner = new Thread(serverHelper);
                            Log.i(TAG, "calling server thread - find product name");
                            runner.start();
                        } else {
                            product.setProductName(productWithBarcodeInInventory.getName());
                        }
                        Log.i(TAG, "Calling Datescanner intent");
                        intent = new Intent(getApplicationContext(), DateScanner.class);
                        startActivityForResult(intent, DATE_REQ_CODE);
                    } else
                        Log.e(TAG, "Failed to get barcode");
                    break;
                case (DATE_REQ_CODE):
                    if (AcitivityResultCode > 0) {
                        String returnedDate = null;
                        if (data != null) {
                            returnedDate = data.getStringExtra("date");
                        }
                        Log.i(TAG, "Got scanned date: " + returnedDate);
                        product.setExpirationDate(returnedDate);
                        if(product.getName() != null) {
                            addProductToDB();
                            break;
                        }
                        String serverResponse;
                        Toast.makeText(getApplicationContext(), "Waiting for server reply, up to 3 seconds.", Toast.LENGTH_LONG).show();
                        Log.d(TAG, "Getting barcode name response from server");
                        serverResponse = (String) serverHelper.getResult();
                        if (serverResponse.equals("-2")) {
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        serverResponse = (String) serverHelper.getResult();
                        if (Integer.valueOf(serverResponse) < 0) {
                            Log.e(TAG, "failed to retrieve product name");
                            Toast.makeText(getApplicationContext(), "Failed to get data from server", Toast.LENGTH_LONG).show();
                            product_name = "";
                        } else {
                            product_name = serverResponse;
                        }
                        intent = new Intent(getApplicationContext(), ManualProductName.class);
                        intent.putExtra("Name", product_name);
                        startActivityForResult(intent, NAME_REQ_CODE);
                    } else
                        Log.e(TAG, "Failed to get date");
                    break;
                case (NAME_REQ_CODE):
                    String name = null;
                    if (data != null) {
                        name = data.getStringExtra("name");
                        product.setProductName(name);
                        addProductToDB();
                    }

                    break;
                default:
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void addProductToDB() {
        if (!dbHelper.addProduct(product.getBarcode(), product.getName(), 1, 1, product.getExpirationDate())) {
            Log.e(TAG, "Failed to insert " + product.toString() + " to database");
            Toast.makeText(getApplicationContext(), "A problem occured while inserting product " +
                    "to database, please try again", Toast.LENGTH_LONG).show();
        }
        populateListView();
    }
}
