package com.example.fresh;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import com.googlecode.tesseract.android.TessBaseAPI;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCamera2View;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.KeyPoint;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Contains static variables used throughout the program.
 */
public class StaticVars extends AppCompatActivity {
    public static final String TAG = "Fresh";
    public static final String DATA_PATH = Environment.getExternalStorageDirectory().toString() + "/Tess";
    public static final String TESS_DATA = "/tessdata";

    public static final class DateFormat{
        // DD/MM
        public static final int NO_YEAR = 0;
        // DD/MM/YY
        public static final int SHORT_YEAR_YY=1;
        // DD/MM/YYYY
        public static final int LONG_YEAR_YYYY=2;
    }

    public static final class ServerConfigurations{
        public static final String SERVER_IP = "192.168.1.58";
        public static final int SERVER_PORT = 9000;
    }
}