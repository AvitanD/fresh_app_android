package com.example.fresh;

import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.fresh.add_product.Product;
import com.example.fresh.utils.DatabaseHelper;

public class sql_tester extends AppCompatActivity {
    EditText barcode, name, amount, quantity, exp_date;
    DatabaseHelper mDBHelper;
    private Button send_button, get_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sql_tester);
        barcode = findViewById(R.id.barcode);
        name = findViewById(R.id.name);
        amount = findViewById(R.id.ammount);
        quantity = findViewById(R.id.quantity);
        exp_date = findViewById(R.id.date);
        mDBHelper = new DatabaseHelper(getApplicationContext());
        send_button = findViewById(R.id.send_button);
        send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Get the data from the textviews and send to db (do it from the scan product activity)
                mDBHelper.addProduct("12345","milk",4,5,"12/1/13");
            }
        });
        get_button = findViewById(R.id.get_button);
        get_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String product_name = name.getText().toString();
                try {
                    //TODO : Get all information from db and put into listview

                    Product product = mDBHelper.getProduct(product_name);
                } catch (CursorIndexOutOfBoundsException e){
                    Toast.makeText(getApplicationContext(), "Couldn't fetch product " + product_name, Toast.LENGTH_LONG).show();
                    Log.e(StaticVars.TAG, e.getMessage());
                }

            }
        });
    }
}
